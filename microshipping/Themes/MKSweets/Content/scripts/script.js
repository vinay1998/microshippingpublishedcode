$(function () {

  $(".imgin").each(function () {
    if ($(this).find('img').length === 0) {
      $(this).html('<img src="' + $(this).attr('imgin') + '">');
    }
  });

  function productLoadImg() {
    $(".itemImg").each(function () {
      if ($(this).find('img').length === 0) {
        $(this).append('<img src="' + $(this).attr('imgmarchent') + '">');
      }
    });
  }

  function merchangePlroductShow() {
    if ($(".itemImg").eq(0).find('img').length === 0) {
      $(".itemImg").eq(0).append('<img src="' + $(".itemImg").eq(0).attr('imgmarchent') + '">');
    }
    if ($(".itemImg").eq(1).find('img').length === 0) {
      $(".itemImg").eq(1).append('<img src="' + $(".itemImg").eq(1).attr('imgmarchent') + '">');
    }
    if ($(".itemImg").eq(2).find('img').length === 0) {
      $(".itemImg").eq(2).append('<img src="' + $(".itemImg").eq(2).attr('imgmarchent') + '">');
    }
    if ($(".itemImg").eq(3).find('img').length === 0) {
      $(".itemImg").eq(3).append('<img src="' + $(".itemImg").eq(3).attr('imgmarchent') + '">');
    }
    if ($('.cartEmptyBox img').length === 0) {
      $('.cartEmptyBox').prepend("<img src='" + $('.cartEmptyBox').attr("emptycartimg") + "'>");
    }
    if ($('.imgMerchentHeader .merchentHImg img').length === 0) {
      $('.imgMerchentHeader .merchentHImg').html("<img src='" + $('.imgMerchentHeader').attr('imgMerchentHeader') + "'>");
    }
  }



  $(window).scroll(function () {
    $('.imginclick').each(function () {
      if ($(this).find('img').length === 0) {
        $(this).html('<img src="' + $(this).attr('imginclick') + '">');
      }
    });
    //$("#nominate_register").css({"background":"url(/Themes/MKSweets/Content/img/banner_reg.jpg)"})
    //$('#playstore').css({"background":"url(/Themes/MKSweets/Content/img/rest_pocket_bg.png)","background-size": "cover"});
    if ($('#playstore .andriod-ph img').length === 0) {
      $('#playstore .andriod-ph').html('<img src="/Themes/MKSweets/Content/img/andriod-phn.png" alt="">')
    }
    productLoadImg();
  })

  merchangePlroductShow();

  $(window).ajaxComplete(function () {
    merchangePlroductShow();
    //productLoadImg();
    $('.imginclick').each(function () {
      if ($(this).find('img').length === 0) {
        $(this).html('<img src="' + $(this).attr('imginclick') + '">');
      }
    });
  })


  $("#time").click(function () {
    $("#timeList").toggle();
  });

  $("#addrs").click(function () {
    $("#addrsDrop").toggle();
  });

  function checkWinHi() {
    var winHi = window.innerHeight - 120;
    $(".home-page-body .master-column-wrapper").css({ "min-height": winHi + "px" });
  }
  checkWinHi();
  $(window).resize(function () {
    checkWinHi();
  });
});

// My Account Tab Script in My Account Page

function hideAccount() {
  $('#myOrderContent').hide();
  $('#personalInfoContent').hide();
  $('#manageAddrsContent').hide();
  $('#paymentsContent').hide();
  $('#helpContent').hide();
  $('#myOrder').removeClass("active");
  $('#personalInfo').removeClass("active");
  $('#manageAddrs').removeClass("active");
  $('#payments').removeClass("active");
  $('#help').removeClass("active");

}

$(function () {
  var selectedAccount = "personalInfo";
  hideAccount();
  $('#personalInfoContent').show("slow");
  $('#personalInfo').addClass("active");

  $('ul.accList li').on('click', function (event) {
    event.preventDefault();
    var slect = $(this).attr('id');

    if (selectedAccount != slect) {

      hideAccount();
      switch (slect) {
        case 'myOrder': $('#myOrderContent').toggle('slow');
          break;
        case 'personalInfo': $('#personalInfoContent').toggle('slow');
          break;
        case 'manageAddrs': $('#manageAddrsContent').toggle('slow');
          break;
        case 'payments': $('#paymentsContent').toggle('slow');
          break;
        case 'help': $('#helpContent').toggle('slow');
          break;

      }
      $(this).addClass("active");
      selectedAccount = slect;
    }
  });
});

function hideAccountAccordian() {
  $('div#myOrderContent').css("display", "none");
  $('div#personalInfoContent').css("display", "none");
  $('div#manageAddrsContent').css("display", "none");
  $('div#paymentsContent').css("display", "none");
  $('div#helpContent').css("display", "none");
}
$(function () {

  $("div.myOrderOption").on('click', function (event) {
    hideAccountAccordian();
    if ($('div#myOrderContent').css('display') == 'block') {
      $('div#myOrderContent').css("display", "none");
    } else {
      $('div#myOrderContent').css("display", "block");
    }
  });

  $("div.personalInfoOption").on('click', function (event) {
    hideAccountAccordian();
    if ($('div#personalInfoContent').css('display') == 'block') {
      $('div#personalInfoContent').css("display", "none");
    } else {
      $('div#personalInfoContent').css("display", "block");
    }
  });

  $("div.manageAddrsOption").on('click', function (event) {
    hideAccountAccordian();
    if ($('div#manageAddrsContent').css('display') == 'block') {
      $('div#manageAddrsContent').css("display", "none");
    } else {
      $('div#manageAddrsContent').css("display", "block");
    }
  });

  $("div.paymentsOption").on('click', function (event) {
    hideAccountAccordian();
    if ($('div#paymentsContent').css('display') == 'block') {
      $('div#paymentsContent').css("display", "none");
    } else {
      $('div#paymentsContent').css("display", "block");
    }
  });

  $("div.helpOption").on('click', function (event) {
    hideAccountAccordian();
    if ($('div#helpContent').css('display') == 'block') {
      $('div#helpContent').css("display", "none");
    } else {
      $('div#helpContent').css("display", "block");
    }
  });
});
// Tab Script
$(function () {
  // tabbed content
  // http://www.entheosweb.com/tutorials/css/tabs.asp
  $(".tabContent").show();
  $(".tabContent:first").show();

  /* if in tab mode */
  $("ul.buttonTabs li").click(function () {

    $(".tabContent").hide();
    //// add - for inactive
    $("ul.buttonTabs li.active img").attr("src", "images/plusIcon.png")
    ////
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();

    $("ul.buttonTabs li").removeClass("active");
    $(this).addClass("active");
    ////// add + for active
    $("ul.buttonTabs li.active img").attr("src", "images/minusIcon.png")
    //////
    $(".tab_drawer_heading").removeClass("d_active");
    $(".tab_drawer_heading[rel^='" + activeTab + "']").addClass("d_active");

  });

  /* if in drawer mode */
  $(".tab_drawer_heading").click(function () {

    $(".tabContent").hide();
    //// add - for inactive
    $(".tab_drawer_heading.d_active img").attr("src", "images/plusIcon.png")
    ////
    var d_activeTab = $(this).attr("rel");
    $("#" + d_activeTab).fadeIn();

    $(".tab_drawer_heading").removeClass("d_active");
    $(this).addClass("d_active");
    ////// add + for active
    $(".tab_drawer_heading.d_active img").attr("src", "images/minusIcon.png")
    //////
    $("ul.buttonTabs li").removeClass("active");
    $("ul.buttonTabs li[rel^='" + d_activeTab + "']").addClass("active");
  });

  /* Extra class "tab_last" 
     to add border to right side
     of last tab */
  $('ul.buttonTabs li').last().addClass("tab_last");

  // TAB FUNCTIONALITY ENDS HERE
});


$(function () {
  $(".foodMenuBar ul.menuList li:first-child").addClass("active");

  var stickyTopSections = $('.foodMenuSection, .checkOutBlock');

  var stickyTop = function () {
    var scrollTop = $(window).scrollTop();
    $('.foodMenuSection').each(function () {
      var $this = $(this);
      if (scrollTop > 435) {
        $this.addClass('sticky');
      }
      else {
        $this.removeClass('sticky');
      }
    });
  };
  var stickyTop1 = function () {
    var scrollTop = $(window).scrollTop();
    $('.checkOutBlock').each(function () {
      var $this = $(this);
      if (scrollTop > 470) {
        $this.addClass('sticky');
      }
      else {
        $this.removeClass('sticky');
      }
    });
  };

  stickyTop();

  $(window).scroll(function () {
    stickyTop();
    stickyTop1();
  });

  var ajaxReq = false;
  $('.product-box-add-to-cart-button').live("click", function () {
    ajaxReq = true;
  });
  $(document).ajaxComplete(function () {
    if (ajaxReq) {
      $('#flyout-cart').load('/shoppingcart/LoadFlyoutcartselector');
      ajaxReq = false;
    }
    $(".miniCartRightFilter .numberOfItems span").html($(".checkOutBlock .itemList .bar").length);
    checkEmpty();
  });
  function checkEmpty() {
    if ($(".miniCartRightFilter .numberOfItems span").html() > "0") {
      if ($(".miniCartRightFilter .numberOfItems span").html() == "1") {
        $(".miniCartRightFilter .numberOfItems").html('<span>1</span> item');
        $(".headerCart a span").html('1');
      } else {
        cartCount();
      }
      $(".miniCartRightFilter > h2").html("My Basket");
      $(".checkOutBlock,.miniCartRightFilter .numberOfItems,.orderScheduleClick").show();
      $(".cartEmptyBox").hide();
    } else {
      cartCount();
      $(".miniCartRightFilter > h2").html("Empty Basket");
      $(".checkOutBlock,.miniCartRightFilter .numberOfItems,.orderScheduleClick").hide();
      $(".cartEmptyBox").show();
      
    }

  }
  checkEmpty();

  function cartCount() {
    var cartCount = 0;
    $(".checkOutBlock .itemList .bar").each(function () {
      if (typeof $(this).find('small').html() !== 'undefined') {
        cartCount = cartCount + parseInt($(this).find('small').html());
      }
    });
    $(".miniCartRightFilter .numberOfItems").html("<span>" + cartCount + "</span> items");
    $(".headerCart a span").html(cartCount);
    if (cartCount == 0) {
      $(".itemList .bar").each(function () {
        if (typeof $(this).find('small').html() !== 'undefined') {
          cartCount = cartCount + parseInt($(this).find('small').html());
        }
      });
      $(".headerCart a").addClass("noItem");
      $(".headerCart a span").html(cartCount);

    }
    if (cartCount > 0) {
      $(".ItemsText").html("<span>" + cartCount + "</span> items");
      $(".headerCart a").removeClass("noItem");
    }
    else {
      $(".ItemsText").html('');
    }
  }

  $(".main-cat-row").each(function (i) {
    $(this).addClass("mainCatNo" + i);
    var ccl = $(".mainCatNo" + i + " " + "> .itemBox").length;
    $(".mainCatNo" + i + " " + ">.col-md-12 .numberOfItems span").html(ccl);
  });

  $(".sub-cat-row").each(function (i) {

    $(this).addClass("subCatNo" + i);
    var ccl = $(".subCatNo" + i + " " + "> .itemBox").length;
    $(".subCatNo" + i + " " + ">.col-md-12 .numberOfItems span").html(ccl);
  });
  $(".catIdL").each(function () {
    //$(this).parents(".itemBox").siblings(".col-md-12").find("h2").attr({"ccCATiD":$(this).attr("catIdtt")});
    //$(this).parents(".itemBox").parent(".menuItemsList").attr({"id":"mId"+$(this).attr("catIdtt")})
  });
  $(".machentCategoryList > li > a").each(function () {
    var leftCatListId = $(this).attr("idL");
    var leftCatListText = $(this).html();
    $(".marchentTopFilter h2.parentCt").each(function () {
      if ($(this).attr("cccatid") === leftCatListId) {
        $(this).html(leftCatListText);
      }
    });
  });
  $(".machentCategoryList .subCategory > li > a").each(function () {
    var leftCatListIdS = $(this).attr("idL");
    var leftCatListTextS = $(this).html();
    $(".marchentTopFilter .subCt").each(function () {
      if ($(this).attr("cccatid") === leftCatListIdS) {
        $(this).html(leftCatListTextS);
      }
    });
  });
  $(".itemImg img").each(function () {
    var ccD = $(this).attr("src");
    var xxt = ccD.split("default-image")[1];
    if (xxt !== undefined) {
      $(this).parents(".menuItemsList").addClass("haveNoImg");

    } else {
      //$(this).parents(".itemBox").parent(".menuItemsList").addClass("haveImg");
    }
  });

  $('.topAddressSelection .dropdown-menu li').on('click', function () {
    var getValue = $(this).html() + '<span class="caret"></span>';
    $('.topAddressSelection .dropdown-select').html(getValue);
    $(this).parent("ul").slideUp();
  });
  $(".topAddressSelection #dLabel").click(function () {
    $(this).siblings("ul").slideDown();
  });

  //$(".bestsellers .numberOfItems span").html($(".bestsellers .itemBox").length);
  if ($(".bestsellers .itemBox").length == 1) {
    $(".bestsellers .numberOfItems").html("<span>" + $(".bestsellers .itemBox").length + "</span> item");
  } else {
    $(".bestsellers .numberOfItems").html("<span>" + $(".bestsellers .itemBox").length + "</span> items");
  }

  //$("#bestsellers .marchentTopFilter h2").html("BESTSELLERS");
  $(".itemBox .productTypeImgCn img").each(function () {
    var ccImgT = $(this).attr("src");
    if ($(this).parents(".productTypeImgCn").siblings(".product-title").find('img').length === 0) {
      $(this).parents(".productTypeImgCn").siblings(".product-title").append("<img src='" + ccImgT + "'/>");
    }
	});
	
	$(".numberOfItems span").each(function(){
		if($(this).html()=="0"){
			$(this).parent(".numberOfItems").hide();
		}
	});
	
	
	
	$("#billing-address-select option").each(function(){
		var adressV = $(this).attr("value");
		var adressH = $(this).html();
		if(adressV ==""){
			$(".addressLists").append('<div class="newAddressBox" value="'+adressV+'" >'+ adressH+'<button class="newAddressBtn">Add Address</button></div>');
		}else{
			$(".addressLists").append('<div class="addressB" value="'+adressV+'" >'+ adressH+'<button class="addressBtnT">Delivery here</button></div>');
		}
		
	});
	
	$(".addressLists .addressB .addressBtnT").click(function(){
		$("#billing-new-address-form").hide();
		var cchbv = $(this).parent(".addressB").attr("value");
		$("#billing-address-select option").each(function(){
			if($(this).attr("value")==cchbv){
				$(this).attr('selected', true);
			}
		});
	});
	$(".newAddressBtn").click(function(){
		$("#billing-new-address-form").show();
	});
	
	function miniCartMobileC(){
		if(window.innerWidth < 992){
			$(".checkOutSection").addClass("checkMiniCartMobileOpen");
		}else{
			$(".checkOutSection").removeClass("checkMiniCartMobileOpen");
		}
	}
	miniCartMobileC();
	
	if(window.location.href === window.location.origin+"/login/checkoutasguest?returnUrl=%2Fcart"){
		window.location.href = window.location.origin+"/onepagecheckout"
	}
	
	function DeviceTopicons(){
    if (window.innerWidth < 992) {
      //$(".mobileMenuItem").animate({ "bottom": "60px" });
      setTimeout(function () {
        $(".mobileMenuItem").animate({ "bottom": "80px" });
      }, 500);

      $(".mobileMenuItem").click(function () {
        $(".leftSideCategoryL").addClass("activeMo");
        $(".machentCategoryList").toggleClass("activeLm");
        $(".menuOverLay").toggleClass("activeOL");
        $("body").toggleClass("menuActiveM");
        if ($("body .menuOverLay").length === 0) {
          $(".leftSideCategoryL").prepend('<div class="menuOverLay"></div>');
        }
      });
      $(document).on("click", ".leftBoxMerchant .menuOverLay", function () {
        $(".leftSideCategoryL").removeClass("activeMo");
        $(".machentCategoryList").removeClass("activeLm");
        $("body").removeClass("menuActiveM");
        $(this).remove();
      });
      $(document).on("click", ".machentCategoryList li a", function () {
        $(".leftSideCategoryL").removeClass("activeMo");
        $(".machentCategoryList").removeClass("activeLm");
        $("body").removeClass("menuActiveM");
        $(".leftBoxMerchant .menuOverLay").remove();
      });
      $(document).on("click", ".checkOutSection .checkOutC1", function () {
        if ($(".checkOutSection .catOpt").length === 0) {
          $(this).parent(".checkOutSection").prepend('<div class="catOpt"></div>');
        }
        $("body").addClass("deviceCartActive");
        $(this).parent(".checkOutSection").addClass("catOpenDevic");
        $(this).parent(".checkOutSection").removeClass("checkMiniCartMobileOpen");
      });
      $(document).on("click", ".catOpt", function () {
        $('body').removeClass("deviceCartActive");
        $(".checkOutSection").removeClass("catOpenDevic");
        $(".checkOutSection").addClass("checkMiniCartMobileOpen");
        $('.tooltripInfo').removeClass('active');
        $('.customToolTripBox').slideUp();
        $(this).remove();
      });

      /*$(".marchentTopFilter h2").each(function(){
        var bdcat = $(this).attr("cccatid");
        var ctVl = $(this).next(".numberOfItems").find("span").html();
        $(".machentCategoryList li a").each(function(){
          var ccidlDv = $(this).attr("idl");
          if(bdcat == ccidlDv){
            $(this).append('<span>'+ctVl+'</span>')
          }
        });
      }); */

      if ($("body .rightUsersDetails").length === 0) {
        $(".restaurInfo").prepend('<div class="rightUsersDetails"><div class="searchIconTopM"></div><div class="deviceUserIcon"></div><div class="deviceUserPr"></div>');
        if ($("body .logoutTxt").length === 0) {
          $(".deviceUserPr").append('<ul><li><a href="#" target="_blank">Contact us</a></li><li><a href="/agentregister">Agent Register</a></li><li><a href="/register">Register</a></li><li><a href="/login">Log in</a></li></ul>');
        } else {
          $(".deviceUserPr").append('<ul><li><a href="#" target="_blank">Contact us</a></li><li class="userNa">' + $(".navbar-nav .custNam").html() + '</li><li><a href="/logout">Log out</a></li></ul>');
        }
      }

      $(window).ajaxComplete(function () {
        $('.searchFilterLeft .backBtnSeach').remove();
        $('.searchFilterLeft input').animate({ "width": "0px", "padding": "0px", "right": "0" });
        $('.searchAndTags').next('.backBtnSeach').remove();
        $('.searchAndTags + .searchOverLay').remove();
      });

    } else {
      $(".rightUsersDetails").remove();
    }
  }
  DeviceTopicons();
  $(window).resize(function () {
    DeviceTopicons();
  });

  $('body .searchIconTopM').unbind('click').click(function () {
    if ($('.searchAndTags + .searchOverLay').length === 0) {
      $('.searchAndTags').after('<div class="searchOverLay"></div>');
    }

    $('.searchFilterLeft input').animate({ "width": window.innerWidth + "px", "padding": "5px 5px 5px 50px", "right": "0" });
    if ($('.searchFilterLeft .backBtnSeach').length === 0) {
      $('.searchFilterLeft').append('<div class="backBtnSeach"></div>');
    }
  });

  $(document).on('click', '.searchFilterLeft .backBtnSeach', function () {
    $(this).remove();
    $('.searchFilterLeft input').animate({ "width": "0px", "padding": "0px", "right": "0" });
    $('.searchAndTags').next('.backBtnSeach').remove();
    $('.searchAndTags + .searchOverLay').remove();
  });


  $('body .deviceUserIcon').unbind('click').click(function () {
    if ($(this).hasClass('active')) {
      $(this).removeClass('active');
      $(".deviceUserPr").slideUp('slow');
    } else {
      $(this).addClass('active');
      $(".deviceUserPr").slideDown('slow');
    }
  });


  $('.checkOutBlock .itemList .bar span').each(function () {

  });

  function setMerchanetPage() {
    $(".main-cat-row").each(function (i) {
      $(this).addClass("mainCatNo" + i);
      var ccl = $(".mainCatNo" + i + " " + "> .itemBox").length;
      if (ccl === 1) {
        $(".mainCatNo" + i + " " + ">.col-md-12 .numberOfItems").html("<span>" + ccl + "</span> item");
      } else {
        $(".mainCatNo" + i + " " + ">.col-md-12 .numberOfItems").html("<span>" + ccl + "</span> items");
      }

    });

    $(".sub-cat-row").each(function (i) {
      $(this).addClass("subCatNo" + i);
      var ccl = $(".subCatNo" + i + " " + "> .itemBox").length;
      if (ccl === 1) {
        $(".subCatNo" + i + " " + ">.col-md-12 .numberOfItems").html("<span>" + ccl + "</span> item");
      } else {
        $(".subCatNo" + i + " " + ">.col-md-12 .numberOfItems").html("<span>" + ccl + "</span> items");
      }

    });
    $(".catIdL").each(function () {
      //$(this).parents(".itemBox").siblings(".col-md-12").find("h2").attr({"ccCATiD":$(this).attr("catIdtt")});
      //$(this).parents(".itemBox").parent(".menuItemsList").attr({"id":"mId"+$(this).attr("catIdtt")})
    });
    $(".machentCategoryList > li > a").each(function () {
      var leftCatListId = $(this).attr("idL");
      var leftCatListText = $(this).html();
      $(".marchentTopFilter h2.parentCt").each(function () {
        if ($(this).attr("cccatid") === leftCatListId) {
          $(this).html(leftCatListText);
        }
      });
    });
    $(".machentCategoryList .subCategory > li > a").each(function () {
      var leftCatListIdS = $(this).attr("idL");
      var leftCatListTextS = $(this).html();
      $(".marchentTopFilter .subCt").each(function () {
        if ($(this).attr("cccatid") === leftCatListIdS) {
          $(this).html(leftCatListTextS);
        }
      });
    });
    $(".itemImg img").each(function () {
      var ccD = $(this).attr("src");
      var xxt = ccD.split("default-image")[1];
      if (xxt !== undefined) {
        $(this).parents(".menuItemsList").addClass("haveNoImg");

      } else {
        //$(this).parents(".itemBox").parent(".menuItemsList").addClass("haveImg");
      }
    });
    //$(".bestsellers .numberOfItems span").html($(".bestsellers .itemBox").length);

    $(".numberOfItems span").each(function () {
      if ($(this).html() == "0") {
        $(this).parent(".numberOfItems").hide();
        var ccIDhT = $(this).parent(".numberOfItems").siblings("h2").attr("cccatid");
        $(this).parents("#" + ccIDhT).hide();
      }
    });

    //$("#bestsellers .marchentTopFilter h2").html("BESTSELLERS");
    /*$(".itemBox .productTypeImgCn img").each(function(){
      //debugger
      var ccImgT = $(this).attr("src");
      if($(this).parents(".productTypeImgCn").siblings(".product-title").next(".prodcutTagProp").length ===0){
        $(this).parents(".productTypeImgCn").siblings(".product-title").after("<span class='prodcutTagProp'><img src='"+ccImgT+"'/></span>");
      }
    }); */
  }

  function miniCartAndQty() {
    //$('.menuItemsBlock .menuItemsList .itemInfo').removeClass('incartProduct');
    /*$(".checkOutBlock .itemList .bar .product span").each(function(){
      //debugger
      var cartProductName = $(this).html().trim().toLowerCase();
      var cartValCln = $(this).siblings('.cartCount').clone();
      $('.menuItemsBlock .menuItemsList .itemInfo .product-title').each(function(){
        var leftProductName = $(this).html().trim().toLowerCase();
        if(leftProductName == cartProductName){
          $(this).parent('.itemInfo').addClass('incartProduct');
          $(this).parent('.itemInfo').find(".cartCount").remove();
          $(this).parent('.itemInfo').find(".prices").after(cartValCln);
        }
      });
        	
    });
  	
    $(".incartProduct .product-title").each(function(){
      var ccrCartL = $(this).html().trim().toLowerCase();
      if($(this).next(".prodcutTagProp").length===1){
        var ccTagImgCfl = $(this).next(".prodcutTagProp").find("img").clone();
      }
    	
      $(".checkOutBlock .itemList .bar .product span").each(function(){
        if($(this).html().trim().toLowerCase() == ccrCartL){
          $(this).prepend(ccTagImgCfl);
        }
      })
    }) */
    $('.menuItemsBlock .menuItemsList .itemInfo .product-title span').each(function () {
      var cctitlele = $(this).text().trim().toLowerCase();
      var cctitlele1 = cctitlele.slice(0, 20);
      var cctitleleR = cctitlele1.replace(/ /g, 'N');
      var cctitleleR1 = cctitleleR.replace(']', 'N');
      var cctitleleR2 = cctitleleR1.replace('[', 'N');
      $(this).parent(".product-title").addClass(cctitleleR2);
    });
    $(".checkOutBlock .itemList .bar .product span").each(function () {
      var cctitlele = $(this).text().trim().toLowerCase();
      var cctitlele1 = cctitlele.slice(0, 20);
      var cctitleleR = cctitlele1.replace(/ /g, 'N');
      var cctitleleR1 = cctitleleR.replace(']', 'N');
      var cctitleleR2 = cctitleleR1.replace('[', 'N');
      $(this).addClass(cctitleleR2);
    });

    $(".checkOutBlock .itemList .bar .product span").each(function () {
      var ccountpLMN = $(this).next(".cartCount").html();
      var ccLcls = $(this).attr("class");
      $('.menuItemsBlock .menuItemsList .itemInfo .product-title.' + ccLcls).each(function () {
        $(this).parent('.itemInfo').addClass('incartProduct');

        $(this).parent('.itemInfo').find(".prices").next('.cartCount').remove();
        $(this).parent('.itemInfo').find(".prices").after('<div class="cartCount">' + ccountpLMN + '</div>');

        var miniCartImg = $(this).find("img").clone();
        var checkCartImg = $('.checkOutBlock .itemList .bar .product span.' + ccLcls + ' ' + 'img').length
        if (checkCartImg === 0) {
          $('.checkOutBlock .itemList .bar .product span.' + ccLcls).prepend(miniCartImg);
        }
      });
    });

    $(".incartProduct .product-title").each(function () {
      var removeCheckhl = $('.checkOutBlock .itemList .bar .product span.' + $(this).attr("class").split('product-title ')[1])
      if (removeCheckhl.length === 0) {
        $(this).parent('.itemInfo').removeClass('incartProduct');
        $(this).parent('.itemInfo').find('.cartCount').remove();
      }
    })

    if ($('.checkOutBlock .itemList .bar .product span img').length === 0) {
      $('.checkOutBlock .itemList').addClass("noProductIconImg");
    } else {
      $('.checkOutBlock .itemList').removeClass("noProductIconImg");
    }

  }

  $(".prodcutTagProp:empty").remove();
  miniCartAndQty();
  setMerchanetPage();

  function setSchudleTimeDate() {
    if ($('.itemListAarea .product').length > 0) {
      if ($('#inputSchBookingDate').val() !== "") {
        var datesadule = $('#inputSchBookingDate').val();
        var timesadule = $('#ddlSchTimings option:selected').html();
        if (timesadule !== "--Select--") {
          $('.miniCartRightFilter h2 span').remove();
          $('.miniCartRightFilter h2').append("<span>Date: " + datesadule + " Time: " + timesadule + "</span>");
        }
      }
    } else {
      $('.miniCartRightFilter h2 span').remove();
    }
  }



  setSchudleTimeDate()
  $(document).ajaxComplete(function () {
    setMerchanetPage();
    miniCartAndQty();
    setSchudleTimeDate()
  });


  //$(window).scroll(function () {
  //  if ($(this).scrollTop() > 0) {
  //    $('#restaurInfoSectionArea').addClass("sticky");
  //  }
  //  else {
  //    $('#restaurInfoSectionArea').removeClass("sticky");      
  //  }
  //});


  function TooltripShow(yourClassTool, toolTripVal) {

    $(yourClassTool).tooltip({ title: toolTripVal, html: true, placement: "auto" });
  }

  TooltripShow("#playstore li a", "Coming Soon");

  function tooltripPos(currentOffset) {
    var tooltripPos = window.innerHeight - $(currentOffset).offset().top;
    if (tooltripPos > 247) {
      $(".customToolTripBox").removeClass("topToolTripShow");
      $(".customToolTripBox").addClass("bottomToolTripShow");
    } else {
      $(".customToolTripBox").removeClass("bottomToolTripShow");
      $(".customToolTripBox").addClass("topToolTripShow");
    }
  }

  function customToolTrip() {
    $(".toPay.totalText .cart-total-left .tooltripInfo").unbind().click(function () {

      if ($(this).next('.customToolTripBox').length === 0) {
        $(this).after(`<div class="customToolTripBox">
        <div class="custtomToolTripData">`+ $(this).attr('custom-title') + `</div>
      <div class="customToolTripArrow"></div>
    </div>`);
      }
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).next('.customToolTripBox').slideUp();
      } else {
        $(this).addClass('active');
        //debugger
        tooltripPos($(this));
        $(this).next('.customToolTripBox').slideDown();
      }
    });
  }

  $(window).resize(function () {
    tooltripPos(".toPay.totalText .cart-total-left .tooltripInfo");
  });

  customToolTrip();

  $(window).ajaxComplete(() => {
    customToolTrip();
  });

  function checkDelTakeAway() {
    if ($('.deliveryTakeBox li input[checked=checked] + label').html() == "Takeaway") {
      $("#dv-billing-add-selected .step-title").html('Billing address <img src="/Themes/MKSweets/Content/img/greenCheckImg1.png">')
    }
    if ($('.deliveryTakeBox li input[checked=checked] + label').html() == "Delivery") {
      $("#dv-billing-add-selected .step-title").html('Delivery Address <img src="/Themes/MKSweets/Content/img/greenCheckImg1.png">')
    }
  }
  checkDelTakeAway();

  $(document).on('click', '.deliveryTakeBox li label', function () {
    checkDelTakeAway();
  });

  function homepageRatingData() {
    $(".merchentRatingTimeList .ratingli").each(function () {
      let notActiveStar = 5 - parseInt($(this).find('em').html());
      let curentRate = parseInt($(this).find('em').html());
      $(this).find("i").remove();
      if ($(this).find("i").length === 0) {
        for (let j = 0; j < notActiveStar; j++) {
          $(this).prepend('<i class="fa fa-star"></i>');
        }
        for (let i = 0; i < curentRate; i++) {
          $(this).prepend('<i class="fa fa-star activeRating"></i>');
        }
      }
    });
  }

  homepageRatingData();

  $(window).ajaxComplete(function () {
    homepageRatingData();
  });



  if ($(".page").hasClass('order-list-page')) {
    function customPagination(pgnyClass) {

      let totalNumberItem = $(pgnyClass).length;
      console.log(totalNumberItem <= 5);
      if (totalNumberItem <= 5) {
        $(pgnyClass).each(function () {
          $(this).show()
        })
      } else {
        let perPageNub = parseInt(totalNumberItem / 5);
        $(pgnyClass).parents('.page-body').append("<ul class='paginationDiv'></ul>");
        for (let i = 1; i < perPageNub; i++) {
          $('body .paginationDiv').append('<li pageVal ="' + i * 5 + '" >' + i + '</li>');
        }
        if (window.location.pathname == "/order/history") {
          $(pgnyClass).eq(5).prevAll().show();
          $(".paginationDiv li:first-child").addClass("selected");
        }
      }
      //window.location.href+
    }
    customPagination(".order-list-page .order-item");
    $(document).on("click", '.paginationDiv li', function () {
      //debugger
      let currentPos = $(this).attr('pageval');
      $(".paginationDiv li").removeClass("selected");
      $(this).addClass('selected');
      let checkLastPag = $('.paginationDiv li').last().hasClass("selected");
      if (checkLastPag === false) {
        if (currentPos > 5) {
          $(".order-list-page .order-item").eq(currentPos).prevAll().show();
          $(".order-list-page .order-item").eq(currentPos).show();
          $(".order-list-page .order-item").eq(currentPos).nextAll().hide();
          $(".order-list-page .order-item").eq(currentPos - 5).prevAll().hide();
        } else {
          $(".order-list-page .order-item").eq(5).prevAll().show();
          $(".order-list-page .order-item").eq(currentPos).show();
          $(".order-list-page .order-item").eq(5).nextAll().hide();
        }
      } else {
        $(".order-list-page .order-item").eq(currentPos).prevAll().hide();
        $(".order-list-page .order-item").eq(currentPos).show();
        $(".order-list-page .order-item").eq(currentPos).nextAll().show();
      }

      $('html,body').animate({
        scrollTop: $('body').offset().top
      }, 1000);

    })
  }

  if ($("body #dv-rating-review-container").length > 0) {
    if ($("#dv-rating-review-container").html().length > 0) {
      $(".ratingYourPrevOrder").show();
      $("body").addClass('ratingYourPrevOrderActive');
      $(document).on("click", ".ratingYourPrevOrder .prevOrderText", function () {
        $("#dv-rating-review-container").show();
        $(".ratingYourPrevOrder").hide();
      });
      $(document).on("click", ".prevOrderClose", function () {
        $("#dv-rating-review-popup .close").click();
        $(".ratingYourPrevOrder").hide();
      })
    } else {
      $(".ratingYourPrevOrder").hide();
      $("body").removeClass('ratingYourPrevOrderActive');
    }
  }



  if ($("html").hasClass("userIsLogin")) {
    if (window.location.pathname == "/login" || window.location.pathname == "/register") {
      window.history.back();
    }
  }
  
});

$("#dv-rating-review-popup .close, #saveTipAmount, #applydiscountcouponcode").bind('click', function () {
  $('body').removeClass('noScroll');
  $('body').addClass('scroll');
  $('body').style.overflow = 'auto !important';
  document.querySelector("body").style.overflow = 'auto !important';
  document.getElementsByClassName("notAndroid23").style.overflow = "auto !important";
});
function onlyNumberKey(evt) {

  // Only ASCII charactar in that range allowed
  var ASCIICode = (evt.which) ? evt.which : evt.keyCode
  if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
    return false;
  return true;

}


$(document).ready(function () {
  var showChar = 150;
  var ellipsestext = "...";
  var moretext = "View More";
  var lesstext = "View Less";
  $('.more').each(function () {
    var content = $(this).html();

    if (content.length > showChar) {
      var c = content.substr(10, showChar);
      var h = content.substr(showChar - 1, content.length - showChar);
      var html = c + '<span class="moreelipses">' + ellipsestext + '</span> &nbsp; <span class="morecontent"><span>' + h + '</span> <a href="" class="morelink">' + moretext + '</a></span>';
      $(this).html(html);
    }

  });

  $(".morelink").click(function () {
    if ($(this).hasClass("less")) {
      $(this).removeClass("less");
      $(this).html(moretext);
    } else {
      $(this).addClass("less");
      $(this).html(lesstext);
    }
    $(this).parent().prev().toggle();
    $(this).prev().toggle();
    return false;
  });






});







//$(document).ready(function () {


//    var SETTINGS = {
//        navBarTravelling: false,
//        navBarTravelDirection: "",
//        navBarTravelDistance: 150
//    }

//    var colours = {
//        0: "#867100",
//        1: "#7F4200",
//        2: "#99813D",
//        3: "#40FEFF",
//        4: "#14CC99",
//        5: "#00BAFF",
//        6: "#0082B2",
//        7: "#B25D7A",
//        8: "#00FF17",
//        9: "#006B49",
//        10: "#00B27A",
//        11: "#996B3D",
//        12: "#CC7014",
//        13: "#40FF8C",
//        14: "#FF3400",
//        15: "#ECBB5E",
//        16: "#ECBB0C",
//        17: "#B9D912",
//        18: "#253A93",
//        19: "#125FB9",
//    }

//    document.documentElement.classList.remove("no-js");
//    document.documentElement.classList.add("js");

//    // Out advancer buttons
//    var pnAdvancerLeft = document.getElementById("pnAdvancerLeft");
//    var pnAdvancerRight = document.getElementById("pnAdvancerRight");
//    // the indicator
//    var pnIndicator = document.getElementById("pnIndicator");

//    var pnProductNav = document.getElementById("pnProductNav");
//    var pnProductNavContents = document.getElementById("pnProductNavContents");

//    pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));

//    // Set the indicator
//    moveIndicator(pnProductNav.querySelector("[aria-selected=\"true\"]"), colours[0]);

//    // Handle the scroll of the horizontal container
//    var last_known_scroll_position = 0;
//    var ticking = false;

//    function doSomething(scroll_pos) {
//        pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
//    }

//    pnProductNav.addEventListener("scroll", function () {
//        last_known_scroll_position = window.scrollY;
//        if (!ticking) {
//            window.requestAnimationFrame(function () {
//                doSomething(last_known_scroll_position);
//                ticking = false;
//            });
//        }
//        ticking = true;
//    });


//    pnAdvancerLeft.addEventListener("click", function () {
//        // If in the middle of a move return
//        if (SETTINGS.navBarTravelling === true) {
//            return;
//        }
//        // If we have content overflowing both sides or on the left
//        if (determineOverflow(pnProductNavContents, pnProductNav) === "left" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
//            // Find how far this panel has been scrolled
//            var availableScrollLeft = pnProductNav.scrollLeft;
//            // If the space available is less than two lots of our desired distance, just move the whole amount
//            // otherwise, move by the amount in the settings
//            if (availableScrollLeft < SETTINGS.navBarTravelDistance * 2) {
//                pnProductNavContents.style.transform = "translateX(" + availableScrollLeft + "px)";
//            } else {
//                pnProductNavContents.style.transform = "translateX(" + SETTINGS.navBarTravelDistance + "px)";
//            }
//            // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
//            pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
//            // Update our settings
//            SETTINGS.navBarTravelDirection = "left";
//            SETTINGS.navBarTravelling = true;
//        }
//        // Now update the attribute in the DOM
//        pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
//    });

//    pnAdvancerRight.addEventListener("click", function () {
//        alert("test1")
//        // If in the middle of a move return
//        if (SETTINGS.navBarTravelling === true) {
//            return;
//        }
//        // If we have content overflowing both sides or on the right
//        if (determineOverflow(pnProductNavContents, pnProductNav) === "right" || determineOverflow(pnProductNavContents, pnProductNav) === "both") {
//            // Get the right edge of the container and content
//            var navBarRightEdge = pnProductNavContents.getBoundingClientRect().right;
//            var navBarScrollerRightEdge = pnProductNav.getBoundingClientRect().right;
//            // Now we know how much space we have available to scroll
//            var availableScrollRight = Math.floor(navBarRightEdge - navBarScrollerRightEdge);
//            // If the space available is less than two lots of our desired distance, just move the whole amount
//            // otherwise, move by the amount in the settings
//            if (availableScrollRight < SETTINGS.navBarTravelDistance * 2) {
//                pnProductNavContents.style.transform = "translateX(-" + availableScrollRight + "px)";
//            } else {
//                pnProductNavContents.style.transform = "translateX(-" + SETTINGS.navBarTravelDistance + "px)";
//            }
//            // We do want a transition (this is set in CSS) when moving so remove the class that would prevent that
//            pnProductNavContents.classList.remove("pn-ProductNav_Contents-no-transition");
//            // Update our settings
//            SETTINGS.navBarTravelDirection = "right";
//            SETTINGS.navBarTravelling = true;
//        }
//        // Now update the attribute in the DOM
//        pnProductNav.setAttribute("data-overflowing", determineOverflow(pnProductNavContents, pnProductNav));
//    });

//    pnProductNavContents.addEventListener(
//        "transitionend",
//        function () {
//            // get the value of the transform, apply that to the current scroll position (so get the scroll pos first) and then remove the transform
//            var styleOfTransform = window.getComputedStyle(pnProductNavContents, null);
//            var tr = styleOfTransform.getPropertyValue("-webkit-transform") || styleOfTransform.getPropertyValue("transform");
//            // If there is no transition we want to default to 0 and not null
//            var amount = Math.abs(parseInt(tr.split(",")[4]) || 0);
//            pnProductNavContents.style.transform = "none";
//            pnProductNavContents.classList.add("pn-ProductNav_Contents-no-transition");
//            // Now lets set the scroll position
//            if (SETTINGS.navBarTravelDirection === "left") {
//                pnProductNav.scrollLeft = pnProductNav.scrollLeft - amount;
//            } else {
//                pnProductNav.scrollLeft = pnProductNav.scrollLeft + amount;
//            }
//            SETTINGS.navBarTravelling = false;
//        },
//        false
//    );

//    // Handle setting the currently active link
//    pnProductNavContents.addEventListener("click", function (e) {
//        var links = [].slice.call(document.querySelectorAll(".pn-ProductNav_Link"));
//        links.forEach(function (item) {
//            item.setAttribute("aria-selected", "false");
//        })
//        e.target.setAttribute("aria-selected", "true");
//        // Pass the clicked item and it's colour to the move indicator function
//        moveIndicator(e.target, colours[links.indexOf(e.target)]);
//    });

//    // var count = 0;
//    function moveIndicator(item, color) {
//        var textPosition = item.getBoundingClientRect();
//        var container = pnProductNavContents.getBoundingClientRect().left;
//        var distance = textPosition.left - container;
//        var scroll = pnProductNavContents.scrollLeft;
//        pnIndicator.style.transform = "translateX(" + (distance + scroll) + "px) scaleX(" + textPosition.width * 0.01 + ")";
//        // count = count += 100;
//        // pnIndicator.style.transform = "translateX(" + count + "px)";

//        if (color) {
//            pnIndicator.style.backgroundColor = color;
//        }
//    }

//    function determineOverflow(content, container) {
//        var containerMetrics = container.getBoundingClientRect();
//        var containerMetricsRight = Math.floor(containerMetrics.right);
//        var containerMetricsLeft = Math.floor(containerMetrics.left);
//        var contentMetrics = content.getBoundingClientRect();
//        var contentMetricsRight = Math.floor(contentMetrics.right);
//        var contentMetricsLeft = Math.floor(contentMetrics.left);
//        if (containerMetricsLeft > contentMetricsLeft && containerMetricsRight < contentMetricsRight) {
//            return "both";
//        } else if (contentMetricsLeft < containerMetricsLeft) {
//            return "left";
//        } else if (contentMetricsRight > containerMetricsRight) {
//            return "right";
//        } else {
//            return "none";
//        }
//    }

//    /**
//    * @fileoverview dragscroll - scroll area by dragging
//    * @version 0.0.8
//    *
//    * @license MIT, see https://github.com/asvd/dragscroll
//     * @copyright 2015 asvd <heliosframework@gmail.com>
//        */


//    (function (root, factory) {
//        if (typeof define === 'function' && define.amd) {
//            define(['exports'], factory);
//        } else if (typeof exports !== 'undefined') {
//            factory(exports);
//        } else {
//            factory((root.dragscroll = {}));
//        }
//    }(this, function (exports) {
//        var _window = window;
//        var _document = document;
//        var mousemove = 'mousemove';
//        var mouseup = 'mouseup';
//        var mousedown = 'mousedown';
//        var EventListener = 'EventListener';
//        var addEventListener = 'add' + EventListener;
//        var removeEventListener = 'remove' + EventListener;
//        var newScrollX, newScrollY;

//        var dragged = [];
//        var reset = function (i, el) {
//            for (i = 0; i < dragged.length;) {
//                el = dragged[i++];
//                el = el.container || el;
//                el[removeEventListener](mousedown, el.md, 0);
//                _window[removeEventListener](mouseup, el.mu, 0);
//                _window[removeEventListener](mousemove, el.mm, 0);
//            }

//            // cloning into array since HTMLCollection is updated dynamically
//            dragged = [].slice.call(_document.getElementsByClassName('dragscroll'));
//            for (i = 0; i < dragged.length;) {
//                (function (el, lastClientX, lastClientY, pushed, scroller, cont) {
//                    (cont = el.container || el)[addEventListener](
//                        mousedown,
//                        cont.md = function (e) {
//                            if (!el.hasAttribute('nochilddrag') ||
//                                _document.elementFromPoint(
//                                    e.pageX, e.pageY
//                                ) == cont
//                            ) {
//                                pushed = 1;
//                                lastClientX = e.clientX;
//                                lastClientY = e.clientY;

//                                e.preventDefault();
//                            }
//                        }, 0
//                    );

//                    _window[addEventListener](
//                        mouseup, cont.mu = function () { pushed = 0; }, 0
//                    );

//                    _window[addEventListener](
//                        mousemove,
//                        cont.mm = function (e) {
//                            if (pushed) {
//                                (scroller = el.scroller || el).scrollLeft -=
//                                    newScrollX = (- lastClientX + (lastClientX = e.clientX));
//                                scroller.scrollTop -=
//                                    newScrollY = (- lastClientY + (lastClientY = e.clientY));
//                                if (el == _document.body) {
//                                    (scroller = _document.documentElement).scrollLeft -= newScrollX;
//                                    scroller.scrollTop -= newScrollY;
//                                }
//                            }
//                        }, 0
//                    );
//                })(dragged[i++]);
//            }
//        }


//        if (_document.readyState == 'complete') {
//            reset();
//        } else {
//            _window[addEventListener]('load', reset, 0);
//        }

//        exports.reset = reset;
//    }));

//})
