﻿var AjaxFilter = {
  filterItems: function (pageNumber) {
    displayAjaxLoading(true);
    var SelectedImageTags = '';
    var Keywords = $("input:text[name='txtKeywords']").val();
    $(".dv-image-tags-filter").each(function () {
      if ($(this).hasClass('img-tag-select')) {
        SelectedImageTags += $(this).attr('id').split('_')[1] + ',';
      }
    });
    var objData = {
      keywords: "",//Keywords,
      selectedImageTags: SelectedImageTags,
      pageNumber: pageNumber,
      categoryId: 0,//$("#categoryId").val(),
      brandNameIds: "",//$("#brandNameIds").val(),
      packSizeIds: "",//$("#packSizeIds").val(),
      minPrice: 0,//$("#slider-3").slider("values", 0),
      maxPrice: 0,//$("#slider-3").slider("values", 1),
      sortingOrder: $("#storingOrder").val(),
      viewMode: $("#hdViewMode").val()
    };
    console.log(objData);
    $.ajax({
      cache: false,
      url: "/Product/FilterItems",
      data: objData,
      type: 'post',
      success: this.success_process,
      complete: this.resetLoadWaiting,
      error: this.ajaxFailure
    });
  },
  filterByImageTags: function (tag) {
    displayAjaxLoading(true);
    $("#dvImageTags_"+tag).addClass('img-tag-select');
    this.filterItems();
  },
  removeKeywordsFilter: function (filterName) {
    displayAjaxLoading(true);
    $("input:text[name='txtKeywords']").val('');
    this.filterItems();
  },
  removeFilter: function (tagId) {
    displayAjaxLoading(true);
    $("#dvImageTags_" + tagId).removeClass('img-tag-select');
    this.filterItems();
  },
  clearAllFilter: function () {
    displayAjaxLoading(true);
    $("input:text[name='txtKeywords']").val('');
    $(".dv-image-tags-filter").each(function () {
      if ($(this).hasClass('img-tag-select')) {
        $(this).removeClass('img-tag-select');
      }
    });
    this.filterItems();
  },
  success_process: function (response) {
    $("#dvItems").html(response);
    displayAjaxLoading(false);
  },
  resetLoadWaiting: function () {
    displayAjaxLoading(false);
  },
  ajaxFailure: function () {
    displayAjaxLoading(false);
    alert('Failed to filter products. Please refresh the page and try one more time.');
  }
}