﻿/*
** nopCommerce ajax cart implementation
*/


var AjaxCart = {
    loadWaiting: false,
    usepopupnotifications: false,
    topcartselector: '',
    topwishlistselector: '',
    flyoutcartselector: '',

    init: function (usepopupnotifications, topcartselector, topwishlistselector, flyoutcartselector) {
        this.loadWaiting = false;
        this.usepopupnotifications = usepopupnotifications;
        this.topcartselector = topcartselector;
        this.topwishlistselector = topwishlistselector;
        this.flyoutcartselector = flyoutcartselector;
    },

    setLoadWaiting: function (display) {
        displayAjaxLoading(display);
        this.loadWaiting = display;
    },

    //add a product to the cart/wishlist from the catalog pages
    addproducttocart_catalog: function (urladd) {
        if (this.loadWaiting != false) {
            return;
        }
        this.setLoadWaiting(true);
      var arr = urladd.split("/");
      var productId = parseInt(arr[3]);
      var shoppingCartType = parseInt(arr[4]);
      var quantity = parseInt($("#cartQuantity_" + productId).val());
      if (quantity > 1 && shoppingCartType == 1) {
        urladd = '/addproducttocart/catalog/' + productId + '/1/' + quantity;
        $("#cartQuantity_" + productId).val('1');
      }
        $.ajax({
            cache: false,
            url: urladd,
            type: "POST",
            success: this.success_process,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },
    //Add Item to cart Mah

    addproductitemtocart_catalog: function (urladd) {
        if (this.loadWaiting != false) {
            return;
        }
      this.setLoadWaiting(true);
      var arr = urladd.split("/");
      var productId = parseInt(arr[3]);
      var shoppingCartType = parseInt(arr[4]);
      var quantity = parseInt($("#cartQuantity_" + productId).val());
      if (quantity > 1 && shoppingCartType == 1) {
        urladd = '/addproductitemtocart/catalog/' + productId + '/1/' + quantity;
        $("#cartQuantity_" + productId).val('1');
      }
        $.ajax({
            cache: false,
            url: urladd,
            type: 'post',
            success: this.success_process,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },

    //Remove Item From Cart Add By Mah
    removeproductfromtocart_catalog: function (urladd) {
        if (this.loadWaiting != false) {
            return;
        }
        this.setLoadWaiting(true);

        $.ajax({
            cache: false,
            url: urladd,
            type: 'post',
            success: this.success_process,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },


    //remove a product to the cart/wishlist from the catalog pages Add by Mah
    removeproducttocart_catalog: function (urladd) {
        if (this.loadWaiting != false) {
            return;
        }
        this.setLoadWaiting(true);
        //alert(urladd);
        $.ajax({
            cache: false,
            url: urladd,
            type: 'post',
            success: this.success_process,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },

    //add a product to the cart/wishlist from the product details page
  addproducttocart_details: function (urladd, formselector) {
        if (this.loadWaiting != false) {
            return;
        }
        this.setLoadWaiting(true);
    var arr = urladd.split("/");
    var productId = parseInt(arr[3]);
    var shoppingCartType = parseInt(arr[4]);
    var quantity = parseInt($("#cartQuantity_" + productId).val());
    if (quantity > 1 && shoppingCartType == 1) {
      urladd = '/addproducttocart/details/' + productId + '/1/' + quantity;
      $("#cartQuantity_" + productId).val('1');
    }
        $.ajax({
            cache: false,
            url: urladd,
            data: $(formselector).serialize(),
            type: "POST",
            success: this.success_process,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
  },
  addItemFromWishListToCart: function (urladd) {
    if (this.loadWaiting != false) {
      return;
    }
    this.setLoadWaiting(true);
        $.ajax({
            cache: false,
            url: urladd,
            type: 'post',
          success: function (response) {
              if (response.updatetopcartsectionhtml) {
                $(AjaxCart.topcartselector).html(response.updatetopcartsectionhtml);
              }
              if (response.updatetopwishlistsectionhtml) {
                $(AjaxCart.topwishlistselector).html(
                  response.updatetopwishlistsectionhtml);
              }
              if (response.updateflyoutcartsectionhtml) {
                $(".js-flyout-cart").each(function () {

                  $(this).replaceWith(response.updateflyoutcartsectionhtml);
                });
              }
              if (response.message) {
                //display notification
                if (response.success == true) {
                  //success
                  if (AjaxCart.usepopupnotifications == true) {
                    displayPopupNotification(response.message, 'success', true);
                  }
                  else {
                    //specify timeout for success messages
                    displayBarNotification(response.message, 'success', 3500);
                  }
                }
                else {
                  //error
                  if (AjaxCart.usepopupnotifications == true) {
                    displayPopupNotification(response.message, 'error', true);
                  }
                  else {
                    //no timeout for errors
                    displayBarNotification(response.message, 'error', 0);
                  }
                }
                //location.href = "/homemerchant";
                return true;
              }
            },
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
  },
  deleteproductfromcart_shoppingcart: function (id, addWishListLink) {
    if (this.loadWaiting != false) {
      return;
    }
    this.setLoadWaiting(true);
    $.ajax({
      url: '/ShoppingCart/DeleteProductFromWishList',
      type: 'POST',
      dataType: 'json',
      cache: false,
      data: { productId: id },
      success: function (result) {
        $('.heart-' + id).removeClass('active');
        $(".wishlistlink_" + id).attr("onclick", "addToWhishList('" + addWishListLink + "', 'heart-" + id + "');return false;");
        displayBarNotification(result.message, 'error', 3500);
        if (result.updatetopwishlistsectionhtml) {
          $(AjaxCart.topwishlistselector).html(result.updatetopwishlistsectionhtml);
        }
        if ($('.heart-' + id).hasClass('wishlist-remove')) {        
          //$('#wishlist-remove-' + id).html('');
          $('#wishlist-remove-' + id).remove();
          $('.menuItemBox_' +id).addClass("item-remove");
        }
      },
      complete: this.resetLoadWaiting,
      error: function (xhr, ajaxOptions, thrownError) {
        alert('Failed');
      }
    });
  },
    //add a product to compare list
    addproducttocomparelist: function (urladd) {
        if (this.loadWaiting != false) {
            return;
        }
        this.setLoadWaiting(true);

        $.ajax({
            cache: false,
            url: urladd,
            type: "POST",
            success: this.success_process,
            complete: this.resetLoadWaiting,
            error: this.ajaxFailure
        });
    },

    success_process: function (response) {
        if (response.updatetopcartsectionhtml) {
            $(AjaxCart.topcartselector).html(response.updatetopcartsectionhtml);
        }
      if (response.updatetopwishlistsectionhtml) {
            $(AjaxCart.topwishlistselector).html(response.updatetopwishlistsectionhtml);
        }
      if (response.updateflyoutcartsectionhtml) {
        $(".js-flyout-cart").each(function () { 
          
          $(this).replaceWith(response.updateflyoutcartsectionhtml);
      });
        }
        if (response.message) {
            //display notification
            if (response.success == true) {
                //success
                if (AjaxCart.usepopupnotifications == true) {
                    displayPopupNotification(response.message, 'success', true);
                }
                else {
                    //specify timeout for success messages
                    displayBarNotification(response.message, 'success', 3500);
                }
            }
            else {
                //error
                if (AjaxCart.usepopupnotifications == true) {
                    displayPopupNotification(response.message, 'error', true);
                }
                else {
                    //no timeout for errors
                    displayBarNotification(response.message, 'error', 0);
                }
            }
            return false;
        }
        if (response.redirect) {
            location.href = response.redirect;
            return true;
        }
        return false;
    },

  resetLoadWaiting: function () {
        AjaxCart.setLoadWaiting(false);
    },

    ajaxFailure: function () {
        alert('Failed to add the product. Please refresh the page and try one more time.');
    }
};