/*
** nopCommerce one page checkout
*/


var Checkout = {
    loadWaiting: false,
    failureUrl: false,

    init: function (failureUrl) {
        this.loadWaiting = false;
        this.failureUrl = failureUrl;

        Accordion.disallowAccessToNextSections = true;
    },

    ajaxFailure: function () {
        location.href = Checkout.failureUrl;
    },
    
    _disableEnableAll: function (element, isDisabled) {
        var descendants = element.find('*');
        $(descendants).each(function() {
            if (isDisabled) {
                $(this).prop("disabled", true);
            } else {
                $(this).prop("disabled", false);
            }
        });

        if (isDisabled) {
            element.prop("disabled", true);
        } else {
            $(this).prop("disabled", false);
        }
    },

    setLoadWaiting: function (step, keepDisabled) {
        if (step) {
            if (this.loadWaiting) {
                this.setLoadWaiting(false);
            }
            var container = $('#' + step + '-buttons-container');
            container.addClass('disabled');
            container.css('opacity', '.5');
            this._disableEnableAll(container, true);
            $('#' + step + '-please-wait').show();
        } else {
            if (this.loadWaiting) {
                var container = $('#' + this.loadWaiting + '-buttons-container');
                var isDisabled = (keepDisabled ? true : false);
                if (!isDisabled) {
                    container.removeClass('disabled');
                    container.css('opacity', '1');
                }
                this._disableEnableAll(container, isDisabled);
                $('#' + this.loadWaiting + '-please-wait').hide();
            }
        }
        this.loadWaiting = step;
    },

    gotoSection: function (section) {
        section = $('#opc-' + section);
        section.addClass('allow');
        Accordion.openSection(section);
    },

    back: function () {
        if (this.loadWaiting) return;
        Accordion.openPrevSection(true, true);
    },

  setStepResponse: function (response) {
        if (response.update_section) {
          $('#checkout-' + response.update_section.name + '-load').html(response.update_section.html);
          $('#schedulerpara').show();
        }
        if (response.allow_sections) {
            response.allow_sections.each(function (e) {
                $('#opc-' + e).addClass('allow');
            });
        }

        //TODO move it to a new method
        if ($("#billing-address-select").length > 0) {
            Billing.newAddress(!$('#billing-address-select').val());
        }
        if ($("#shipping-address-select").length > 0) {
            Shipping.newAddress(!$('#shipping-address-select').val());
        }

        if (response.goto_section) {
            Checkout.gotoSection(response.goto_section);
            return true;
        }
        if (response.redirect) {
            location.href = response.redirect;
            return true;
        } 
        return false;
    },
    setStepResponseCustom: function (response) {
        if (response.update_section) {
            $('#checkout-' + response.update_section.name + '-load').html(response.update_section.html);
        }
        //if (response.allow_sections) {
        //    response.allow_sections.each(function (e) {
        //        $('#opc-' + e).addClass('allow');
        //    });
        //}

        //if (response.goto_section) {
        //    Checkout.gotoSection(response.goto_section);
        //    return true;
        //}
        if (response.redirect) {
            location.href = response.redirect;
            return true;
        }
        return false;
    },
    saveOrderNote: function () {
        $.post("Checkout/SaveOrderNote", { orderNote: $("#txtAdditionalNote").val() });
    }
};





var Billing = {
    form: false,
    saveUrl: false,
    disableBillingAddressCheckoutStep: false,

    init: function (form, saveUrl, disableBillingAddressCheckoutStep) {
        this.form = form;
        this.saveUrl = saveUrl;
        this.disableBillingAddressCheckoutStep = disableBillingAddressCheckoutStep;
    },

      newAddress: function (isNew) {
        if (isNew) {
          this.resetSelectedAddress();
          $('#billing-new-address-form').show();
          $("body").addClass("billing-new-address-formActive");
          $("body").addClass("noScroll");
        } else {
          $('#billing-new-address-form').hide();
          $("body").removeClass("billing-new-address-formActive");
        }
        $(document).trigger({ type: "onepagecheckout_billing_address_new" });
      },
      closePopup: function () {
        $('#billing-new-address-form').hide();
        $("body").removeClass("billing-new-address-formActive");
        $("body").removeClass("noScroll");
      },
      selectAddress: function (addressId) {
        $("#hdbilling_address_id").val(addressId);
        if (addressId == '') {
          this.resetSelectedAddress();
          $('#billing-new-address-form').show();
          //       $('#billing-buttons-container').show();
          $("body").addClass("billing-new-address-formActive");
          $("body").addClass("noScroll");
        } else {
          $("#billing-button-continue").click();
          $('#billing-new-address-form').hide();
          $("body").removeClass("billing-new-address-formActive");
          //     $('#billing-buttons-container').hide();
        }
        $.event.trigger({ type: "onepagecheckout_billing_address_new" });
    },
    setAddressType: function (addressTypeId) {
        if ($(".type-option-btn-selected").length > 0) {
            $(".type-option-btn-selected").removeClass('type-option-btn-selected');
        }
        $("#address-type-" + addressTypeId).addClass('type-option-btn-selected');
        $("#hdAddressTypeId").val(addressTypeId);
    },
    resetSelectedAddress: function () {
        var selectElement = $('#billing-address-select');
        if (selectElement) {
            selectElement.val('');
        }
        $(document).trigger({ type: "onepagecheckout_billing_address_reset" }); 
    },

  save: function () {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('billing');
        
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: "POST",
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        //ensure that response.wrong_billing_address is set
        //if not set, "true" is the default value
        if (typeof response.wrong_billing_address == 'undefined') {
            response.wrong_billing_address = false;
        }
        if (Billing.disableBillingAddressCheckoutStep) {
            if (response.wrong_billing_address) {
                Accordion.showSection('#opc-billing');
            } else {
                Accordion.hideSection('#opc-billing');
            }
        }


        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

      if (response.isNewAddress) {
        $(".select-billing-address").prepend(response.newAddress);
      }
      if (response.updateFlyoutShoppingCart) {
        $(".js-flyout-cart").each(function () {

          $(this).html(response.updateFlyoutShoppingCartHtml);
        });
      }
      Billing.closePopup();
        //Billing.initializeCountrySelect();
        if (response.update_section.name != "billing") {
            if ($('#dv-billing-add-selected').length > 0) {
                $('#dv-billing-add-selected').load('/checkout/GetSelectedAddress');
            }
        }
        Checkout.setStepResponse(response);
    },
    getSelectedAddress: function (showSelectedAdd) {
        if (showSelectedAdd) {
            if ($('#dv-billing-add-selected').length > 0) {
                $('#dv-billing-add-selected').load('/checkout/GetSelectedAddress');
            }
        }
        else {
            $('#dv-billing-add-selected').html('<h2 class="title">Add a address</h2>');
            $('#checkout-billing-load').load('/checkout/GetBillingAddress');
        }
    },
    
    initializeCountrySelect: function () {
        if ($('#opc-billing').has('select[data-trigger="country-select"]')) {
            $('#opc-billing select[data-trigger="country-select"]').countrySelect();
        }
    }
};



var Shipping = {
    form: false,
    saveUrl: false,

    init: function (form, saveUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
    },

    newAddress: function (isNew) {
        if (isNew) {
            this.resetSelectedAddress();
            $('#shipping-new-address-form').show();
        } else {
            $('#shipping-new-address-form').hide();
        }
        $(document).trigger({ type: "onepagecheckout_shipping_address_new" });
        //Shipping.initializeCountrySelect();
    },

    togglePickupInStore: function (pickupInStoreInput) {
        if (pickupInStoreInput.checked) {
            $('#pickup-points-form').show();
            $('#shipping-addresses-form').hide();
        }
        else {
            $('#pickup-points-form').hide();
            $('#shipping-addresses-form').show();
        }
    },

    resetSelectedAddress: function () {
        var selectElement = $('#shipping-address-select');
        if (selectElement) {
            selectElement.val('');
        }
        $(document).trigger({ type: "onepagecheckout_shipping_address_reset" });
    },

    save: function () {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('shipping');

        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: "POST",
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }
        Checkout.setStepResponse(response);
    },

    initializeCountrySelect: function () {
        if ($('#opc-shipping').has('select[data-trigger="country-select"]')) {
            $('#opc-shipping select[data-trigger="country-select"]').countrySelect();
        }
    }
};



var ShippingMethod = {
    form: false,
    saveUrl: false,

    init: function (form, saveUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
    },

    validate: function () {
        var methods = document.getElementsByName('shippingoption');
        if (methods.length==0) {
            alert('Your order cannot be completed at this time as there is no shipping methods available for it. Please make necessary changes in your shipping address.');
            return false;
        }

        for (var i = 0; i< methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        alert('Please specify shipping method.');
        return false;
    },
    
    save: function () {
        if (Checkout.loadWaiting != false) return;
        
        if (this.validate()) {
            Checkout.setLoadWaiting('shipping-method');
        
            $.ajax({
                cache: false,
                url: this.saveUrl,
                data: $(this.form).serialize(),
                type: "POST",
                success: this.nextStep,
                complete: this.resetLoadWaiting,
                error: Checkout.ajaxFailure
            });
        }
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        Checkout.setStepResponse(response);
    }
};

//delivery slot fun start
var DeliveryScheduleMethod = {
  form: false,
  saveUrl: false,

  init: function (form, saveUrl) {
    this.form = form;
    this.saveUrl = saveUrl;
  },

  validate: function (value) {
    var slotId = $('#SlotId').val();
    if (value == 1 && (slotId == '' || slotId == 0)) {
      alert("Please select slot first.");
      return false;
    }
    return true;
  },

  save: function (value) {
    if (Checkout.loadWaiting != false) return;
    if (this.validate(value)) {
      Checkout.setLoadWaiting('deliveryschedule-method');
      $.ajax({
        cache: false,
        url: this.saveUrl,
        data: $(this.form).serialize(),
        type: "POST",
        success: this.nextStep,
        complete: this.resetLoadWaiting,
        error: Checkout.ajaxFailure
      });
    }
  },
  resetLoadWaiting: function () {
    Checkout.setLoadWaiting(false);
  },

  nextStep: function (response) {
    if (response.error) {
      if ((typeof response.message) == 'string') {
        alert(response.message);
      } else {
        alert(response.message.join("\n"));
      }

      return false;
    }

    Checkout.setStepResponse(response);
  }
};
//delivery slot fun end

var PaymentMethod = {
    form: false,
    saveUrl: false,

    init: function (form, saveUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
    },

    toggleUseRewardPoints: function (useRewardPointsInput) {
        if (useRewardPointsInput.checked) {
            $('#payment-method-block').hide();
        }
        else {
            $('#payment-method-block').show();
        }
    },

    toggleUseCustomerWallet: function (useCustomerWalletInput) {
        if (useCustomerWalletInput.checked) {
            $('#payment-method-block').hide();
           $('#payment-method-buttons-container').show();
        }
        else {
            $('#payment-method-block').show();
          $('#payment-method-buttons-container').hide();
        }
    },

    validate: function () {
        var methods = document.getElementsByName('paymentmethod');
        if (methods.length == 0) {
            alert('Your order cannot be completed at this time as there is no payment methods available for it.');
            return false;
        }

      //WalletAmount
      var customerWalletAmountEnoughToPayForOrder = document.getElementById('CustomerWalletAmountEnoughToPayForOrder');
      if (customerWalletAmountEnoughToPayForOrder != undefined && customerWalletAmountEnoughToPayForOrder != null && customerWalletAmountEnoughToPayForOrder.value) {
          return true;
        }
        
        for (var i = 0; i < methods.length; i++) {
            if (methods[i].checked) {
                return true;
            }
        }
        alert('Please specify payment method.');
        return false;
    },
    
    save: function () {
        if (Checkout.loadWaiting != false) return;
        
        if (this.validate()) {
            Checkout.setLoadWaiting('payment-method');
            $.ajax({
                cache: false,
                url: this.saveUrl,
                data: $(this.form).serialize(),
                type: "POST",
                success: this.nextStep,
                complete: this.resetLoadWaiting,
                error: Checkout.ajaxFailure
            });
        }
    },
    saveCustom: function () {
        if (Checkout.loadWaiting != false) return;

        if (this.validate()) {
            Checkout.setLoadWaiting('payment-method');
            $.ajax({
                cache: false,
                url: this.saveUrl,
                data: $(this.form).serialize(),
                type: 'post',
                success: this.nextStepCustom,
                complete: this.resetLoadWaiting,
                error: Checkout.ajaxFailure
            });
        }
    },
    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }
      if (response.success) {
          ConfirmOrder.isSuccess = true;
          window.location = ConfirmOrder.successUrl;
        }
        Checkout.setStepResponse(response);
    },
    nextStepCustom: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }
      if (response.success) {
          ConfirmOrder.isSuccess = true;
          window.location = ConfirmOrder.successUrl;
        }
        Checkout.setStepResponseCustom(response);
    }
};



var PaymentInfo = {
    form: false,
    saveUrl: false,

    init: function (form, saveUrl) {
        this.form = form;
        this.saveUrl = saveUrl;
    },

    save: function () {
        if (Checkout.loadWaiting != false) return;
        
        Checkout.setLoadWaiting('payment-info');
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: "POST",
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },
    saveCustom: function () {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('payment-info');
        $.ajax({
            cache: false,
            url: this.saveUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStepCustom,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },
    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }

        Checkout.setStepResponse(response);
    },
    nextStepCustom: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }
        if (response.redirect) {
            ConfirmOrder.isSuccess = true;
            location.href = response.redirect;
            return;
        }
        if (response.success) {
            ConfirmOrder.isSuccess = true;
            window.location = ConfirmOrder.successUrl;
        }
        Checkout.setStepResponseCustom(response);
    }
};



var ConfirmOrder = {
    form: false,
    saveUrl: false,
    isSuccess: false,

    init: function (saveUrl, successUrl) {
        this.saveUrl = saveUrl;
        this.successUrl = successUrl;
    },

    save: function () {
        if (Checkout.loadWaiting != false) return;
        
        //terms of service
        var termOfServiceOk = true;
        if ($('#termsofservice').length > 0) {
            //terms of service element exists
            if (!$('#termsofservice').is(':checked')) {
                $("#terms-of-service-warning-box").dialog();
                termOfServiceOk = false;
            } else {
                termOfServiceOk = true;
            }
        }
        if (termOfServiceOk) {
            Checkout.setLoadWaiting('confirm-order');
            $.ajax({
                cache: false,
                url: this.saveUrl,
                type: "POST",
                success: this.nextStep,
                complete: this.resetLoadWaiting,
                error: Checkout.ajaxFailure
            });
        } else {
            return false;
        }
    },
    
    resetLoadWaiting: function (transport) {
        Checkout.setLoadWaiting(false, ConfirmOrder.isSuccess);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }

            return false;
        }
        
        if (response.redirect) {
            ConfirmOrder.isSuccess = true;
            location.href = response.redirect;
            return;
        }
        if (response.success) {
            ConfirmOrder.isSuccess = true;
            window.location = ConfirmOrder.successUrl;
        }

        Checkout.setStepResponse(response);
    }

};


var LoginSignUp = {
    form: false,
    storeUrl: false,

    init: function (form, storeUrl) {
        this.form = form;
        this.storeUrl = storeUrl;
    },
    showHideLoginSignup: function (showDiv, hideDiv) {
        $("#dv-login-signup-buttons").hide();
        $(hideDiv).hide();
        $(showDiv).show();
    },

    validate: function () {
        var txtPhoneNumber = document.getElementsByName('txtPhoneNumber');
        if (txtPhoneNumber == 0) {
            alert('Please enter phone number.');
            return false;
        }
        else {
            return true;
        }

    },

    submit: function (actionUrl) {
        if (Checkout.loadWaiting != false) return;

        Checkout.setLoadWaiting('login-please-wait');

        $.ajax({
            cache: false,
            url: this.storeUrl + actionUrl,
            data: $(this.form).serialize(),
            type: 'post',
            success: this.nextStep,
            complete: this.resetLoadWaiting,
            error: Checkout.ajaxFailure
        });
    },

    resetLoadWaiting: function () {
        Checkout.setLoadWaiting(false);
    },

    nextStep: function (response) {
        if (response.error) {
            if ((typeof response.message) == 'string') {
                alert(response.message);
            } else {
                alert(response.message.join("\n"));
            }
            return false;
      }
      if (response.changeHeaderLInk) {
        $('#navbarCollapse').replaceWith(response.headerLink);
      }
        if (response.update_section.name == "checkout-login") {
            $('#dv-checkout-login').html(response.update_section.html);
        }
        else if (response.update_section.name == "checkout-signup") {
            $('#dv-checkout-signup').html(response.update_section.html);
        }
        else {
            $('#opc-login-signup').load('/checkout/GetUserDetails');
            $('#flyout-cart').load('/shoppingcart/LoadFlyoutcartselector');
            if ($("#dv-cross-sell-products").length > 0) {
                $("#dv-cross-sell-products").load('/checkout/getcrosssellproducts');
            }
            Checkout.setStepResponse(response);
        }
    },
    getLoggedInDetails: function () {
        $('#opc-login-signup').load('/checkout/GetUserDetails');
    }
};